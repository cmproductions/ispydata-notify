/**
 * Module dependencies.
 */

var express = require('express')
    , http = require('http')
    , path = require('path')
    , sessionredis = require('connect-redis')(express);

var app = express();

/**
 * Express Session and configuration
 */
var sessionredisUrl = require('url').parse(process.env.SESSIONREDIS || "redis://nodejitsu:nodejitsudb8157061277.redis.irstack.com:f327cfe980c971946e80b8e975fbebb4@nodejitsudb8157061277.redis.irstack.com:6379");
var sessionredisAuth = sessionredisUrl.auth.split(':');
var sessionredisParams = { host: sessionredisUrl.hostname, port: sessionredisUrl.port, db: sessionredisAuth[0], pass: sessionredisAuth[1] + ":" + sessionredisAuth[2], ttl: 900 };
console.log("Session Redis - " + require("util").inspect(sessionredisParams));

app.configure(function(){
    app.enable('trust proxy');
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.compress());
    app.use(express.favicon());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({ secret: 'nfiewldncie.ndy', store: new sessionredis(sessionredisParams) }));
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.errorHandler());
    app.use(app.router);
});

/**
 * Express Route for Chat Client
 */
app.get('/', function (req, res) {
    res.sendfile(__dirname + 'public/index.html');
});

var server = http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});

/**
 * Socket.io
 */
var redis = require('redis')
    , io = require('socket.io').listen(server);

var socketredisUrl = require('url').parse(process.env.SOCKETIOREDIS || "redis://nodejitsu:nodejitsudb5852577523.redis.irstack.com:f327cfe980c971946e80b8e975fbebb4@nodejitsudb5852577523.redis.irstack.com:6379");
var socketredisAuth = socketredisUrl.auth.split(':');
var socketredisParams = { host: socketredisUrl.hostname, port: socketredisUrl.port, db: socketredisAuth[0], pass: socketredisAuth[1] + ":" + socketredisAuth[2], ttl: 900 };
console.log("Socket Redis - " + require("util").inspect(socketredisParams));
var pub = redis.createClient(socketredisParams.port, socketredisParams.host);
var sub = redis.createClient(socketredisParams.port, socketredisParams.host);
var store = redis.createClient(socketredisParams.port, socketredisParams.host);
pub.auth(socketredisParams.pass, function(){console.log("pub authed");});
sub.auth(socketredisParams.pass, function(){console.log("sub authed");});
store.auth(socketredisParams.pass, function(){console.log("store authed");});

io.configure( function(){
    io.enable('browser client minification');  // send minified client
	io.enable('browser client etag');          // apply etag caching logic based on version number
	io.enable('browser client gzip');          // gzip the file
	io.set('log level', 1);                    // reduce logging
	io.set('transports', [                     // enable all transports (optional if you want flashsocket)
        'websocket'
        , 'flashsocket'
        , 'htmlfile'
        , 'xhr-polling'
        , 'jsonp-polling'
	]);
	var RedisStore = require('socket.io/lib/stores/redis');
	io.set('store', new RedisStore({redisPub:pub, redisSub:sub, redisClient:store}));
});

io.sockets.on('connection', function (socket) {    
    handleMessageBroadcasting(socket);
    handleRoomJoining(socket);
    handleRoomLeaving(socket);
    handleRoomList(socket);
    handleClientDisconnection(socket);
});

function handleMessageBroadcasting(socket) {
    socket.on('message', function (message) {
        if (message.room)
            socket.broadcast.to(message.room).emit('message', message); 
    });
}

function handleRoomJoining(socket) {
    socket.on('join', function(room) {
        socket.join(room.joinRoom);
        
        if (room.leaveRoom) {
            socket.leave(room.leaveRoom);
        }
        
        socket.emit('joinResult', {room: room.joinRoom});
    });
}

function handleRoomLeaving(socket) {
    socket.on('leave', function(room) {
       socket.leave(room.leaveRoom);
       socket.emit('leaveResult', {room: room.leaveRoom});
    });
}

function handleClientDisconnection(socket) {
    socket.on('disconnect', function() {
        //don't do anything, just disconnect and let them leave
    });
}

function handleRoomList(socket) {
    socket.on('rooms', function() {
        socket.emit('rooms', io.sockets.manager.rooms);
    });
}
