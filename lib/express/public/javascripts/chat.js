/**
 * Created on Cloud9
 * User: matt
 * Date: 9/26/12
 * Time: 6:32 PM
 */
var id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
});

var Chat = function(socket) {
    this.socket = socket;
    this.socket.emit('join', { joinRoom: "Lobby"});
};

Chat.prototype.sendMessage = function(room, text) {
    var message = {
        room: room,
        text: text };
    this.socket.emit('message', message);
};

Chat.prototype.changeRoom = function(currentRoom, newRoom) {
    this.socket.emit('join', {
        joinRoom: newRoom,
        leaveRoom: currentRoom
    });
};

Chat.prototype.processCommand = function(currentRoom, command) {
    var words = command.split(' ')
        , command = words[0].substring(1, words[0].length).toLowerCase()
        , message = false;
    switch(command) {
        case 'join':
            words.shift();
            var newRoom = words.join(' ');
            this.changeRoom(currentRoom, newRoom);
            break;
        default:
            message = 'Unrecognized command.';
            break;
    }
    return message;
};