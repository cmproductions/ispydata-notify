var sugar = require('sugar');
var redis = require('redis');
var channels = Array.create();
var pub, sub;

exports = module.exports = Notify;

exports.PubClient = pub;
exports.SubClient = sub;


function Notify(redisHost, redisPort, redisPassword) {
    sub = redis.createClient(redisPort, redisHost);
    pub = redis.createClient(redisPort, redisHost);
    sub.auth(redisPassword);
    pub.auth(redisPassword);
    sub.on("message", function(channel, message) {
        if (channels[channel])
            channels[channel](message);
    });
};

Notify.prototype.subscribe = function(channel, callback) {
    channels[channel] = callback;
};

Notify.prototype.publish = function(channel, message) {
    pub.publish(channel, message);
};


